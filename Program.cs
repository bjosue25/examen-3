﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

       namespace Decorator
    {
        class Program
        {

            static void Main(string[] args)
            {
                //Paso 1: Defina algunos menu y cuántos de cada uno podemos hacer
                PIZZA PizzaEspecial = new PIZZA("Cebolla", "Queso parmesano", "borde de queso");
                PizzaEspecial.Display();

                Pasta PastaNormal = new Pasta("Pasta diaria recién hecha", "Salsa de tomate");
                PastaNormal.Display();

                Console.WriteLine("\nMenu Disponibles.");

                //Paso 2: Decora los menu; ahora, si intentamos pedirlos una vez que nos quedemos sin ingredientes, podemos notificar al cliente
                Disponible PizzaDisponible = new Disponible(PizzaEspecial, 3);
                Disponible PastaDisponible = new Disponible(PastaNormal, 4);

                //Paso 3: pide un montón de menu disponible al cliente 
                PizzaDisponible.OrderItem("John");
                PizzaDisponible.OrderItem("Sofia");
                PizzaDisponible.OrderItem("Macas");

                PastaDisponible.OrderItem("Sofia");
                PastaDisponible.OrderItem("Francis");
                PastaDisponible.OrderItem("Reyna");
                PastaDisponible.OrderItem("Diana");
                PastaDisponible.OrderItem("Juan");

                PizzaDisponible.Display();
                PastaDisponible.Display();

                Console.ReadKey();
            }
        }
    }


