﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{

    abstract class Restaurante
    {
        public abstract void Display();
    }


    class PIZZA : Restaurante
    {
        private string _verduras;
        private string _queso;
        private string _masa;

        public PIZZA(string verduras, string queso, string masa)
        {
            _verduras = verduras;
            _queso = queso;
            _masa = masa;
        }

        public override void Display()
        {
            Console.WriteLine("\nPIZZA:");
            Console.WriteLine(" Verduras: {0}", _verduras);
            Console.WriteLine(" Queso: {0}", _queso);
            Console.WriteLine(" Masa: {0}", _masa);
        }
    }



    class Pasta : Restaurante
    {
        private string _tipoPasta;
        private string _aderezo;

        public Pasta(string tipoPasta, string aderezo)
        {
            _tipoPasta = tipoPasta;
            _aderezo = aderezo;
        }

        public override void Display()
        {
            Console.WriteLine("\nPasta Clasica:");
            Console.WriteLine(" Pasta: {0}", _tipoPasta);
            Console.WriteLine(" Aderezo: {0}", _aderezo);
        }
    }





    abstract class Decorator : Restaurante
    {
        protected Restaurante _menu;

        public Decorator(Restaurante menu)
        {
            _menu = menu;
        }

        public override void Display()
        {
            _menu.Display();
        }
    }

    /// <summary>
    /// A concrete Decorator. This class will impart "responsibilities" onto the dishes (e.g. whether or not those dishes have enough ingredients left to order them)
    /// </summary>
    class Disponible : Decorator
    {
        public int NumeroDisponible { get; set; } //How many can we make?
        protected List<string> cliente = new List<string>();
        public Disponible(Restaurante menu, int numeroDisponible) : base(menu)
        {
            NumeroDisponible = numeroDisponible;
        }

        public void OrderItem(string nombre)
        {
            if (NumeroDisponible > 0)
            {
                cliente.Add(nombre);
                NumeroDisponible--;
            }
            else
            {
                Console.WriteLine("\nNo hay suficientes ingredientes para " + nombre + " su orden!");
            }
        }

        public override void Display()
        {
            base.Display();

            foreach (var cliente in cliente)
            {
                Console.WriteLine("Orden por " + cliente);
            }
        }
    }
}
